const express = require('express')
const cors = require('cors');
const userRouter = require('./routes/user');
require('./db/database');

const app = express();
app.listen(3000);
console.log('server are running');
// middware
app.use(express.json());
app.use(cors())
// route
app.use('/user', userRouter);
 


    
