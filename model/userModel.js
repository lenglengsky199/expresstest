const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    name:{
        type: String,
        require: true,
    },
    age:{
        type: Number,
        min: 18,
        max: 65,
        require: true
    },
    ceatedAt:{
        type: Date,
        default: Date.now(),
    }
})
module.exports = mongoose.model('users', UserSchema);